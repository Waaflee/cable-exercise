import CloudworkService from "@/lib/mock-service"
import type {
  CancelRequest,
  CreateRequest,
  GetAllResponse,
  Work,
} from "@/lib/types"
import { makeAutoObservable } from "mobx"

const hasCompleted = (work: Work) => {
  return new Date().getTime() >= work.completeDate.getTime()
}

export class AppController {
  private hasInit = false

  private cloudworkClient = new CloudworkService()

  private fetchedData: GetAllResponse | undefined = undefined

  constructor() {
    makeAutoObservable(this)
  }

  get workloads(): Work[] {
    return this.fetchedData?.workloads || []
  }

  init = () => {
    // react strict-mode in development calls useEffects twice
    if (this.hasInit) return
    this.hasInit = true

    // add some dummy data when app boots to get started
    this.fetchedData = {
      workloads: [
        {
          id: 0,
          status: "WORKING",
          completeDate: new Date(),
          complexity: 1,
        },
        {
          id: 1,
          status: "SUCCESS",
          completeDate: new Date(),
          complexity: 1,
        },
        {
          id: 2,
          status: "FAILURE",
          completeDate: new Date(),
          complexity: 1,
        },
      ],
    }
  }

  // FIXME: more consisten error handling through the following methods
  createWorkload = async (params: CreateRequest) => {
    console.log("create workload", params)
    // TODO: better error handling
    try {
      const { work } = await this.cloudworkClient.create(params)
      this.fetchedData?.workloads.push(work)

      // FIXME: maybe it's better to call this.syncWorkload from the WorkloadItem component
      // resync with server after work it's complete
      const refetchDelay =
        work.completeDate.getTime() - new Date().getTime() + 500 // we add a little tolerance

      // TODO: better erro handling
      setTimeout(() => {
        this.syncWorkload(work).catch((error) =>
          console.error("Failed to re sync workload", error)
        )
      }, refetchDelay)
    } catch (error) {
      console.error("Failed to create workload", error)
      // Forward Error so UI can handle it
      throw new Error("Network error creating workload!")
    }
  }

  syncWorkload = async ({ id }: CancelRequest) => {
    console.log("syncing work: " + id)
    // TODO: better error handling
    // maybe we should throw instead of returning undefined
    try {
      const response = await this.cloudworkClient.getWorkload({ id })
      this.updateWorkload(response.work)
      return response.work
    } catch (error) {
      console.error("Failed to sync workload", error)
      // Forward Error so UI can handle it
      throw new Error("Network error syncing workload!")
    }
  }

  cancelWorkload = async (work: Work) => {
    // Here the error handling it's being done by the ui
    // Todo: better error handling
    if (hasCompleted(work)) {
      throw new Error("Workload cannot be canceled")
    }
    try {
      const response = await this.cloudworkClient.cancelWorkload(work)
      this.updateWorkload(response.work)
    } catch (error) {
      // Forward Error so UI can handle it
      throw new Error("Network error canceling workload!")
    }
  }

  updateWorkload = (work: Work) => {
    // we don't have any workloads so nothing to update
    if (!this.fetchedData?.workloads) {
      return undefined
    }

    const indexOfWork = this.fetchedData?.workloads.findIndex(
      (item) => item.id === work.id
    )

    // FIXME: maybe it's better to throw an error than returning "undefined"
    if (typeof indexOfWork === "number") {
      this.fetchedData.workloads[indexOfWork] = work
      return work
    }
    return undefined

    // alternative using Array.prototype.map but:
    // - it's less legible
    // - we have to do additional steps to report if object it's not in array

    // this.fetchedData.workloads = this.fetchedData.workloads.map((item) => {
    //   if (item.id === work.id) {
    //     return { ...work }
    //   }
    //   return item
    // })
  }
}

export default AppController
