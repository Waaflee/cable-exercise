import type { Work } from "@/lib/types"
import { observer } from "mobx-react-lite"
import WorkloadItem from "./WorkloadItem/WorkloadItem"

interface WorkloadListProps {
  workloads: Work[]
  onCancel(work: Work): Promise<void>
}

export const WorkloadList = observer<WorkloadListProps>(
  ({ workloads, onCancel }) => {
    return (
      <ul>
        {workloads.map((work) => (
          <li key={work.id}>
            <WorkloadItem
              work={work}
              onCancel={async (work) => {
                console.log("Cancel workload", { work })
                await onCancel(work)
              }}
            />
          </li>
        ))}
      </ul>
    )
  }
)

export default WorkloadList
