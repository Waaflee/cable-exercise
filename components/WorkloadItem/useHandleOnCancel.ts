import { Work } from "@/lib/types"
import { useCallback, useState } from "react"

const useHandleOnCancel = (onCancel: (work: Work) => Promise<unknown>) => {
  const [isCancelling, setIsCancelling] = useState(false)

  const handleOnCancel = useCallback(
    async (work: Work) => {
      try {
        setIsCancelling(true)
        await onCancel(work)
        setIsCancelling(false)
      } catch (error) {
        setIsCancelling(false)
        window.alert("Failed to cancel workload")
        console.error("Failed to cancel workload", error)
      }
    },
    [onCancel]
  )

  return {
    isCancelling,
    handleOnCancel,
  }
}

export default useHandleOnCancel
