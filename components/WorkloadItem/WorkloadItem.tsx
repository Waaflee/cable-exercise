import type { Work } from "@/lib/types"
import { observer } from "mobx-react-lite"
import moment from "moment"
import { useState } from "react"
import useHandleOnCancel from "./useHandleOnCancel"
import useWorkloadItem from "./useWorkloadItem"

const formatCountdown = (seconds: number) => {
  return moment.utc(seconds * 1000).format("mm:ss")
}

interface WorkloadItemProps {
  work: Work
  onCancel: (work: Work) => Promise<unknown>
}

export const WorkloadItem = observer<WorkloadItemProps>(
  ({ work, onCancel }) => {
    const { remainingTime, hasCompleted } = useWorkloadItem({ work })
    const { isCancelling, handleOnCancel } = useHandleOnCancel(onCancel)

    const workloadInProgress = work.status === "WORKING" && !hasCompleted

    return (
      <div>
        <h3>Workload #{work.id}</h3>
        <div>Complexity: {work.complexity}</div>

        <div>Status: {work.status}</div>
        {work.status === "WORKING" && (
          <div>Remaining Time: {formatCountdown(remainingTime)}</div>
        )}

        {workloadInProgress && (
          <>
            <div>Complete date: {moment(work.completeDate).format()}</div>
            <button
              onClick={() => handleOnCancel(work)}
              disabled={isCancelling}
            >
              Cancel {isCancelling && "..."}
            </button>
          </>
        )}
      </div>
    )
  }
)

export default WorkloadItem
