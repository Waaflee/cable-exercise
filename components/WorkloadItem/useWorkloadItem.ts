import { useEffect, useRef, useState } from "react"
import type { Work } from "@/lib/types"

const getRemainingTimeInSeconds = (work: Work) =>
  Math.ceil((work.completeDate.getTime() - new Date().getTime()) / 1000)

interface UseWorkloadItemArgs {
  work: Work
}

const useWorkloadItem = ({ work }: UseWorkloadItemArgs) => {
  const [remainingTime, setRemainingTime] = useState(
    getRemainingTimeInSeconds(work)
  )
  const interval = useRef<NodeJS.Timer>()

  const hasCompleted = remainingTime === 0

  // Rerender every second to update countdown display
  useEffect(() => {
    interval.current = setInterval(() => {
      setRemainingTime((currentRemainingTime) => {
        return currentRemainingTime >= 1 ? currentRemainingTime - 1 : 0
      })
    }, 1000)

    // clear interval on unmount if not already cleared
    return () => interval.current && clearInterval(interval.current)
  }, [])

  // Clear interval when countdown reaches 0
  useEffect(() => {
    if (hasCompleted && interval.current) {
      clearInterval(interval.current)
      interval.current = undefined
    }
  }, [hasCompleted, remainingTime])

  return {
    remainingTime,
    hasCompleted,
  }
}

export default useWorkloadItem
